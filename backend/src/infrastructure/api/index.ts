import dotenv from 'dotenv';
import express from 'express';
import cors from 'cors';
import { gif } from '@/infrastructure/api/routes';
import { createServer } from 'http';

dotenv.config();
const port = process.env.PORT || 8080;

const app = express();
const server = createServer(app);

app.use(cors({
  origin: process.env.URL_FRONT
}))
app.use('/gifs', gif.search())

app.listen(port, () => {
  console.info(`[INFO] Server running at port ${port}`);
});
