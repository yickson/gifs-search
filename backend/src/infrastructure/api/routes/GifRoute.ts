import express, { IRouter, Request, Response } from 'express';
import { getGif } from '@/infrastructure/api/services/gif.service';
import gifController from '@/infrastructure/api/controllers/gif.controller';

class GifRoute {
  private readonly route: IRouter;
  constructor() {
    this.route = express.Router()
  }

  search(): IRouter {
    return this.route.get('/search', gifController.getGifs)
  }
}

export default GifRoute;

