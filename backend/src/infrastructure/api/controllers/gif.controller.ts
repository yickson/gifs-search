import {Request, Response} from 'express';
import { getGif } from '@/infrastructure/api/services/gif.service';

const gifController = {
  getGifs: async (req: Request, res: Response) => {
    let { q, limit, offset } = req.query;
    if (offset === undefined) {
      offset = '0';
    }
    if (typeof q === 'string' && typeof limit === 'string' && typeof offset === 'string') {
      const data = await getGif(q, limit, offset);
      return res.send({
        ...data,
        message: 'success'
      })
    }
    return res.send({
      data: null,
      message: 'Parameters not provided'
    })
  }
}

export default gifController;
