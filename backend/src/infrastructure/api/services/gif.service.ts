// import fetch from 'node-fetch';

import { GifResponse } from '@/infrastructure/api/interfaces/gif.interface';

export const getGif = async (query: string, limit: string, offset: string): Promise<GifResponse> => {
  const response = await fetch(`https://api.giphy.com/v1/gifs/search?q=${query}&api_key=eVSAobD4deIbHWxCm6bPp6MdQ7Wxsz9v&limit=${limit}&offset=${offset}`);
  return await response.json();
}
