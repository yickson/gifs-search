interface GifInterface {
  id: string;
  title: string;
  url: string;
}

interface GifPagination {
  total_count: number;
  count: number;
  offset: number;
}

interface GifMeta {
  status: number;
  msg: string;
  response_id: string;
}

export interface GifResponse {
  data: GifInterface[],
  pagination: GifPagination;
  meta: GifMeta;
}
