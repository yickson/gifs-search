import server from "@/infrastructure/api/server";
import request from "supertest";
import GifRoute from '@/infrastructure/api/routes/GifRoute';
describe('Server - API Gifs', ()=> {
  beforeAll(() => {
    const gifs = new GifRoute()
    server.use(gifs.search())
  });

  beforeEach(() => {
    jest.clearAllMocks();
  })
  it('should return response gifs', async ()=> {
    const response = await request(server).get("/search?q=house&api_key=eVSAobD4deIbHWxCm6bPp6MdQ7Wxsz9v&limit=10&offset=0");
    const responseData = response.body;
    // Evaluate structure of json
    expect(Object.keys(responseData)).toEqual(['data', 'pagination', 'meta', 'message']);
    // Evaluate state code of API
    expect(response.statusCode).toBe(200);
  })

});
