import React, { useState, useEffect } from 'react';

interface Paginate {
  currentPage: number;
  totalPages: number;
  onPageChange: (val: any) => void
}
// @ts-ignore
export const Pagination = ({ currentPage, totalPages, onPageChange }: Paginate) => {
  const [pageNumbers, setPageNumbers] = useState<(string|number)[]>([]);
  if (totalPages > 4999) {
    totalPages = 4999;
  }
  useEffect(() => {
    const newPageNumbers: (string|number)[] = [];
    if (totalPages <= 9) {
      for (let i = 1; i <= totalPages; i++) {
        newPageNumbers.push(i);
      }
    } else {
      if (currentPage <= 4) {
        for (let i = 1; i <= 5; i++) {
          newPageNumbers.push(i);
        }
        newPageNumbers.push('...');
        newPageNumbers.push(totalPages);
        // totalPages > 4999 ? newPageNumbers.push(4999) : newPageNumbers.push(totalPages);
      } else if (currentPage > 4 && currentPage < totalPages - 4) {
        newPageNumbers.push(1);
        newPageNumbers.push('...');
        for (let i = currentPage - 2; i <= currentPage + 2; i++) {
          newPageNumbers.push(i);
        }
        newPageNumbers.push('...');
        newPageNumbers.push(totalPages);
      } else {
        newPageNumbers.push(1);
        newPageNumbers.push('...');
        for (let i = totalPages - 4; i <= totalPages; i++) {
          newPageNumbers.push(i);
        }
      }
    }
    setPageNumbers(newPageNumbers);
  }, [currentPage, totalPages]);

  return (
    <>
      {Array.isArray(pageNumbers) ? (
        pageNumbers.map((pageNumber, i) => (
          typeof pageNumber === "string" ? (
            <span className="border-2 m-1 border-gray-500 bg-gray-500 text-white p-2 rounded" key={i}>
          {pageNumber}
        </span>
          ) : (
            <span className="border-2 m-1 border-black bg-black text-white p-2 rounded" key={i} onClick={() => onPageChange(pageNumber)}>
          {pageNumber}
        </span>
          )
        ))
      ) : (
        <span>No page numbers available</span>
      )}
    </>
  );
}
