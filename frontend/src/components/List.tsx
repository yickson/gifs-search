import React, { useEffect, useState } from 'react';
import "../index.css"
import { Pagination } from './Pagination';
import { GifCard } from './GifCard';

type Gif = {
    id: string;
    title: string;
    url: string
}


export const List = () => {

    const [data, setData] = useState<Gif[]>([]);
    const [search, setSearch] = useState<string>('');
    const [page, setPage] = useState<number>(1);
    const [totalPages, setTotalPages] = useState<number>(1);
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(true)


    useEffect(() => {
        async function fetchData () {
            setLoading(true)
            try {
                const response = await fetch(`http://localhost:8080/gifs/search?q=${search}&limit=10&offset=${(page - 1) * 10}`);
                let result = await response.json();
                setTotalPages(result.pagination.total_count);
                result = result.data.map((item: any) => {
                    return {
                        id: item.id,
                        title: item.title,
                        url: item.images.original.url
                    }
                });
                setData(result);
            } catch (error) {
                setData([]);
            } finally {
                setLoading(false);
            }
        }
        fetchData();
    }, [search, page]);

    function handlePageChange(newPage: number) {
        setPage(newPage);
    }

    function handleSearch(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();
        setPage(1);
        const searchValue = (event.currentTarget.elements.namedItem('search') as HTMLInputElement).value
        setSearch(searchValue);
    }

    function handleNextPage() {
        setPage(page + 1);
    }

    function handlePreviousPage() {
        setPage(page - 1);
    }

    return (
        <>
            <h1 className=" text-center text-2xl text-blue-600">
                Search Gifs
            </h1>
            <form className="flex justify-center" onSubmit={handleSearch}>
                <input id="search" className="border-2 border-blue-200 rounded" type="text" name="search" placeholder="Search" />
                <button className="border-2 bg-black p-2 rounded text-white" type="submit">Search</button>
            </form>
            {loading && (<div aria-label="Loading..." role="status">
                <svg className="h-6 w-6 animate-spin" viewBox="3 3 18 18">
                    <path
                      className="fill-gray-200"
                      d="M12 5C8.13401 5 5 8.13401 5 12C5 15.866 8.13401 19 12 19C15.866 19 19 15.866 19 12C19 8.13401 15.866 5 12 5ZM3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C7.02944 21 3 16.9706 3 12Z"></path>
                    <path
                      className="fill-gray-800"
                      d="M16.9497 7.05015C14.2161 4.31648 9.78392 4.31648 7.05025 7.05015C6.65973 7.44067 6.02656 7.44067 5.63604 7.05015C5.24551 6.65962 5.24551 6.02646 5.63604 5.63593C9.15076 2.12121 14.8492 2.12121 18.364 5.63593C18.7545 6.02646 18.7545 6.65962 18.364 7.05015C17.9734 7.44067 17.3403 7.44067 16.9497 7.05015Z"></path>
                </svg>
            </div>)}
            {error && (<p>Ups! An Error Occurred</p>)}
            <div className="flex flex-wrap">
                {data && data.map((gif: Gif) =>
                  (
                    <div key={gif.id}>
                        <GifCard title={gif.title} image={gif.url} />
                    </div>
                  ))
                }
            </div>
            { search === '' && (<p> Enter a word to search </p>) }
            <p className="text-black text-base">Page: {page}</p>
            <div className="flex justify-content">
                <button className="border-2 m-1 border-black bg-black text-white p-2 rounded" onClick={handlePreviousPage} disabled={page === 1}>
                    Previous
                </button>
                <Pagination currentPage={page} totalPages={totalPages} onPageChange={handlePageChange} />
                <button className="border-2 m-1 border-black bg-black text-white p-2 rounded" onClick={handleNextPage}>
                    Next
                </button>
            </div>
        </>
    )
}
