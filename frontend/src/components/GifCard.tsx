// import '../css/GifCard.css';

interface Card {
  title: string;
  image: string;
}

export const GifCard = ({title, image}: Card) => {

  function truncateText(text: string) {
    const maxLength = 22;
    if (text.length > maxLength) {
      return text.slice(0, maxLength) + '...';
    } else {
      return text;
    }
  }

  return (
    <>
      <div className="card max-w-sm rounded overflow-hidden shadow-lg my-4 mx-2">
        <img className="w-72 h-72" src={image} alt={title} />
        <p className="text-center text-gray-700 text-base">{truncateText(title)}</p>
      </div>
    </>
  );
}
