import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { List } from './List';

// Stop-Motion Home

test('renders input search', async () => {
  render(<List/>);
  const input: HTMLInputElement = screen.getByPlaceholderText("Search");
  fireEvent.change(input, {target: {value: 'house'}});
  expect(input.value).toBe('house');
  expect(input).toBeInTheDocument();
});
