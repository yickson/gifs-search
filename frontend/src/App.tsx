import { List } from './components/List';

function App() {
  return (
    <div className="flex flex-col justify-center">
      <List />
    </div>
  );
}

export default App;
