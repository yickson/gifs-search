# gifs-search

### Applications
* frontend
* backend

### Backend

```
cd backend
npm install
npm run dev
```

#### Endpoint
```
http://localhost:8080/gifs/search?q=house&api_key=${process.env.API_GIF}&limit=10&offset=0
```

* q = Query search in input or API
* limit=10 default
* offset=0 manage paginate

### Frontend

```
cd frontend
npm install
npm run start
```
